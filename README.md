# My own pd mode
My attempt at a PD mode for SSTV.
#### Useful links for info
* [PD explained by the creator](http://www.classicsstv.com/pdmodes.php)
* [PDF with some info on all modes](http://www.sstv-handbook.com/download/sstv_04.pdf)
* [Info on YCbCr](https://en.wikipedia.org/wiki/YCbCr)
* [YUV/RGB conversion](/home/atisha/Pictures/Screenshot from 2018-05-05 22-12-59.png)
* [An example of a tone "producer"](https://codereview.stackexchange.com/questions/83504/basic-c-tone-generator)
* [A simpler way to explain tones/sounds](https://stackoverflow.com/questions/9137297/generating-sounds-without-a-library)
#### Decoder
* [Java implementation](http://dp.nonoo.hu/projects/ham-dsp-tutorial/22-sstv-decoder/)
* [General info on decoding](http://www.spetzler.dk/bjarke/Projects/Dsp/DSP%20FM%20Demodulator%20for%20SSTV.htm)
* [Filter design](http://www.labbookpages.co.uk/audio/firWindowing.html)
#### Extra comments
* It looks like adding more taps to the LP filter reduces quality for some reason.
The lowest you can go is 11 taps, lower than that and it starts to degrade pretty quickly.

#### Libraries used
* Magick++ (install → sudo apt install libmagick++-dev)