#!/bin/bash
sudo -u $USER_NAME bash
mkdir build
mkdir bin
currentPath=$(pwd)
goalPath=/build
buildDir="$currentPath$goalPath"
cd $buildDir
cmake $currentPath
make install
cd ../bin
./PD
