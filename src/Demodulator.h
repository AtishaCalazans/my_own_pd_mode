//
// Created by atisha on 09.11.18.
//

#ifndef MY_OWN_PD_MODE_DEMODULATOR_H
#define MY_OWN_PD_MODE_DEMODULATOR_H

#include "ModeTable.h"
#include <vector>
#include <array>

class BaseDecoder;

class Demodulator {
public:

    Demodulator();

    Demodulator(sstv_mode mode);

    void run(double sample);

    bool active = false;

private:

    BaseDecoder* m_decoder;
    std::array<double, 51> m_FIRLPcoeffs;
    int m_filter_filled = 0; // keeps track of how much of the LP filter has been filled in, caps at half the array size of m_FIRLPcoeffs

    double m_osc_phase = 0;
    double m_prev_real = 0;         // real part of the previous sample
    double m_prev_imaginary = 0;    // imaginary part of the previous sample
    double m_threshold = 0.0593997; // get demodulated value above 0
    double m_scale = 0.158684;      // 0 -> 2300 Hz, m_scale -> 1500 Hz

    std::vector<double> m_FIRLPlist1;
    std::vector<double> m_FIRLPlist2;

    std::vector<double> m_xvMA1;
    std::vector<double> m_xvMA2;
    double m_yvMA1prev = 0;
    double m_yvMA2prev = 0;

    double FIRLowPass1(double sample);

    double FIRLowPass2(double sample);

    double demodulate(double sample);

    double noiseReductionFilter1(double sampleIn);

    double noiseReductionFilter2(double sampleIn);

};

#endif //MY_OWN_PD_MODE_DEMODULATOR_H
