//
// Created by atisha on 09.11.18.
//
#include "Demodulator.h"
#include "Modulator.h"
#include "Decoder/BaseDecoder.h"
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <cmath>

Demodulator::Demodulator() {
    // read all filter coefficients from a txt file
    std::ifstream coeff_file("../src/Decoder/filter_coefficients.txt");
    if (coeff_file.is_open()) {
        std::string line;
        int i = 0;
        while (std::getline(coeff_file, line)) {
            std::istringstream iss(line);
            double coefficient;
            if (!(iss >> coefficient)) {
                throw std::invalid_argument("Value is not a double");
            } else {
                m_FIRLPcoeffs[i] = coefficient;
            }
            ++i;
        }
    } else {
        throw std::invalid_argument("can't find file");
    }

    for (int i = 0; i < m_FIRLPcoeffs.size(); ++i) {
        m_FIRLPlist1.push_back(0);
        m_FIRLPlist2.push_back(0);
    }
    for (int i = 0; i < 9; ++i) {
        m_xvMA1.push_back(0);
        m_xvMA2.push_back(0);
    }
}

Demodulator::Demodulator(sstv_mode mode) : Demodulator() {
    m_decoder = new BaseDecoder(mode);
}

void Demodulator::run(double sample) {
    // let the filter get half full before useful data can be obtained from it
    if (m_filter_filled < floor(m_FIRLPcoeffs.size() / 2)) {
        demodulate(sample);
        ++m_filter_filled;
    } else {
        m_decoder->run(demodulate(sample));
    }
}

double Demodulator::FIRLowPass1(double sample) {
    for (int i = 0; i < m_FIRLPlist1.size() - 1; ++i) {
        m_FIRLPlist1[i] = m_FIRLPlist1[i + 1];
    }
    m_FIRLPlist1[m_FIRLPlist1.size() - 1] = sample;
    double sum = 0;
    for (int i = 0; i <= m_FIRLPlist1.size() - 1; ++i) {
        sum += m_FIRLPlist1[i] * m_FIRLPcoeffs[i];
    }
    return sum;
}

double Demodulator::FIRLowPass2(double sample) {
    for (int i = 0; i < m_FIRLPlist2.size() - 1; ++i) {
        m_FIRLPlist2[i] = m_FIRLPlist2[i + 1];
    }
    m_FIRLPlist2[m_FIRLPlist2.size() - 1] = sample;
    double sum = 0;
    for (int i = 0; i <= m_FIRLPlist2.size() - 1; ++i) {
        sum += m_FIRLPlist2[i] * m_FIRLPcoeffs[i];
    }
    return sum;
}

double Demodulator::demodulate(double sample) {
    m_osc_phase += TWO_PI / 16.0;

    double real_part = cos(m_osc_phase) * sample;
    double imaginary_part = sin(m_osc_phase) * sample;

    if (m_osc_phase >= TWO_PI) m_osc_phase -= TWO_PI;

    real_part = FIRLowPass1(real_part);
    imaginary_part = FIRLowPass2(imaginary_part);

//    real_part = noiseReductionFilter1(real_part);
//    imaginary_part = noiseReductionFilter2(imaginary_part);

    sample = (imaginary_part * m_prev_real - real_part * m_prev_imaginary) / (real_part * real_part + imaginary_part * imaginary_part);

    m_prev_real = real_part;
    m_prev_imaginary = imaginary_part;

    sample += m_threshold; // bring the value above 0

    int luminance = int((sample/m_scale)*255);
    luminance = 255-luminance;
    if (luminance > 255) luminance = 255;
    if (luminance < 0) luminance = 0;

    double scaled_luminance = luminance / 255.0;

    return scaled_luminance;
}

double Demodulator::noiseReductionFilter1(double sampleIn) {
    for (int i = 0; i < m_xvMA1.size()-1; ++i) {
        m_xvMA1[i] = m_xvMA1[i+1];
    }
    m_xvMA1[m_xvMA1.size()-1] = sampleIn;
    m_yvMA1prev = m_yvMA1prev + m_xvMA1[m_xvMA1.size()-1] - m_xvMA1[0];
    return m_yvMA1prev;
}

double Demodulator::noiseReductionFilter2(double sampleIn) {
    for (int i = 0; i < m_xvMA2.size()-1; ++i) {
        m_xvMA2[i] = m_xvMA2[i+1];
    }
    m_xvMA2[m_xvMA2.size()-1] = sampleIn;
    m_yvMA2prev = m_yvMA2prev + m_xvMA2[m_xvMA2.size()-1] - m_xvMA2[0];
    return m_yvMA2prev;
}
