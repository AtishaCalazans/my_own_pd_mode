#include <iostream>
#include <cmath>
#include <fstream>
#include <Magick++.h>
#include <memory>
#include "Modulator.h"
#include "Demodulator.h"
#include "Encoder/pdModeEnc.h"
#include "ImageWrapper.h"


using namespace Magick;

int main() {
    sstv_mode mode = PD120;
    Demodulator* demod = new Demodulator(mode);
    auto img = std::make_unique<ImageWrapper>("../extra/test_pattern(640x496).png");
    //auto img = std::make_unique<ImageWrapper>("../extra/sergio.png");
    //auto img = std::make_unique<ImageWrapper>("../extra/Colours/White.png");

    pdModeEnc enc(mode, "test.wav");
    enc.demodulator = demod;
    enc.addImagePtr(img);
    enc.writeHeader();
    enc.writeLines();

    return 0;
}