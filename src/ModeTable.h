//
// Created by atisha on 8/4/18.
//

#ifndef MY_OWN_PD_MODE_MODETABLE_H
#define MY_OWN_PD_MODE_MODETABLE_H

#include <string>
#include <map>

enum sstv_mode {
    PD90,
    PD120,
    PD160,
    PD180,
    PD240,
};

struct ModeTable {
    std::string name;
    uint8_t VIS;
    bool parity_bit;
    int image_time;
    int res_x;
    int res_y;
    int sync;
    int porch;
    int pixel_time;
};

static std::map<sstv_mode, ModeTable> MODE_TABLE = {
        {PD90,  {"PD90",  0x63, false, 89989120,  320, 256, 20000, 2080, 532}},
        {PD120, {"PD120", 0x5F, false, 126103040, 640, 496, 20000, 2080, 190}},
        {PD160, {"PD160", 0x62, true,  160883200, 512, 400, 20000, 2080, 382}},
        {PD180, {"PD180", 0x60, true,  187051520, 640, 496, 20000, 2080, 286}},
        {PD240, {"PD240", 0x61, false, 248000000, 640, 496, 20000, 2080, 382}},
};

#endif //MY_OWN_PD_MODE_MODETABLE_H
