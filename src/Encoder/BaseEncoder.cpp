//
// Created by atisha on 8/4/18.
//
#include "BaseEncoder.h"
#include "../Modulator.h"
#include "../ImageWrapper.h"
#include "../ModeTable.h"

BaseEncoder::BaseEncoder(sstv_mode mode, std::string wav_filename) : m_mode(mode) {
    m_mod_ptr = std::make_unique<Modulator>(wav_filename);
}

void BaseEncoder::addImagePtr(std::unique_ptr<ImageWrapper>& img_ptr) {
    m_mod_ptr->demodulator = demodulator;
    m_img_ptr = std::move(img_ptr);
    m_img_ptr_n = std::make_unique<ImageWrapper>(m_img_ptr->getWidth(), m_img_ptr->getHeight());
}

void BaseEncoder::writeHeader() {
    demodulator->active = false;
    m_mod_ptr->write_tone(1900, 300000); //leader tone
    m_mod_ptr->write_tone(1200,  10000); //break
    m_mod_ptr->write_tone(1900, 300000); //leader tone
    m_mod_ptr->write_tone(1200,  30000); //start bit

    uint8_t vis = MODE_TABLE[m_mode].VIS;
    //loop over the bits of the VIS code
    for (int i = 0; i < 7; ++i) {
        //if the current bit read is 1
        if ((vis >> i) & 1) {
            m_mod_ptr->write_tone(1100, 30000);
        } else {
            m_mod_ptr->write_tone(1300, 30000);
        }
    }
    //write the parity bit
    if (MODE_TABLE[m_mode].parity_bit) {
        m_mod_ptr->write_tone(1100, 30000);
    } else {
        m_mod_ptr->write_tone(1300, 30000);
    }

    m_mod_ptr->write_tone(1200,  30000); //stop bit
    demodulator->active = true;
}

void BaseEncoder::writeLines() {
    m_mod_ptr->finalize();
}