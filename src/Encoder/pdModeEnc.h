//
// Created by atisha on 8/4/18.
//

#ifndef MY_OWN_PD_MODE_PDMODE_H
#define MY_OWN_PD_MODE_PDMODE_H

#include "BaseEncoder.h"
#include <vector>

class pdModeEnc : public BaseEncoder{
public:

    pdModeEnc(const sstv_mode mode, const std::string wav_filename);

    void writeLines();

private:

    void writeLine(const int line);

    /**
     * @brief creates a vector with at position i the average of v1[i] and v2[i]
     */
    std::vector<double> calculateAverageVector(std::vector<double> v1, std::vector<double> v2) const;

};

#endif //MY_OWN_PD_MODE_PDMODE_H
