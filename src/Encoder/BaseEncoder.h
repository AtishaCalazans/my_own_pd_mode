//
// Created by atisha on 8/4/18.
//

#ifndef MY_OWN_PD_MODE_BASEENCODER_H
#define MY_OWN_PD_MODE_BASEENCODER_H

#include <string>
#include <memory>
#include "../ModeTable.h"
#include "../Demodulator.h"

class ImageWrapper;
class Modulator;

class BaseEncoder {
public:

    BaseEncoder(sstv_mode mode, std::string wav_filename);

    void addImagePtr(std::unique_ptr<ImageWrapper>& img_ptr);

    void writeHeader();

    virtual void writeLines();

    Demodulator* demodulator;

protected:

    std::unique_ptr<ImageWrapper> m_img_ptr;
    std::unique_ptr<ImageWrapper> m_img_ptr_n;
    std::unique_ptr<Modulator> m_mod_ptr;
    sstv_mode m_mode;

    virtual void writeLine(const int line) = 0;

};

#endif //MY_OWN_PD_MODE_BASEENCODER_H
