//
// Created by atisha on 8/4/18.
//
#include "pdModeEnc.h"
#include "../ImageWrapper.h"
#include "../Modulator.h"
#include "../ModeTable.h"
#include <iostream>

pdModeEnc::pdModeEnc(const sstv_mode mode, const std::string wav_filename) : BaseEncoder(mode, wav_filename) {}

void pdModeEnc::writeLines() {
    int amount_of_lines = std::floor(double(m_img_ptr->getHeight()) / 2.0);
    for (int i = 0; i < amount_of_lines; ++i) {
        writeLine(i);
    }
    BaseEncoder::writeLines();
    m_img_ptr_n->write_to_file("testo.png");
}

void pdModeEnc::writeLine(const int line) {
    demodulator->active = false;
    int img_line = line * 2;
    //get all data needed from the image for this scan line
    std::vector<double> YLine0 = m_img_ptr->getYLine(img_line);
    std::vector<double> YLine1 = m_img_ptr->getYLine(img_line + 1);
    std::vector<double> avgU = calculateAverageVector(m_img_ptr->getULine(img_line), m_img_ptr->getULine(img_line + 1));
    std::vector<double> avgV = calculateAverageVector(m_img_ptr->getVLine(img_line), m_img_ptr->getVLine(img_line + 1));
    //get time for one pixel
    int px_time = MODE_TABLE[m_mode].pixel_time;

    //write sync pulse
    m_mod_ptr->write_tone(1200, MODE_TABLE[m_mode].sync);
    //write porch
    m_mod_ptr->write_tone(1500, MODE_TABLE[m_mode].porch);

    //Y range is [0, 1]
    //U and V range is [-0.5, 0.5]
    //frequency range is [1.5 kHz, 2.3 kHz]

    demodulator->active = true;
    //write Y0
    for (double Y: YLine0) {
        m_mod_ptr->write_tone(1500 + (Y * 800), px_time);
    }
    //write V
    for (double V: avgV) {
        m_mod_ptr->write_tone(1500 + ((V + 0.5) * 800), px_time);
    }
    //write U
    for (double U: avgU) {
        m_mod_ptr->write_tone(1500 + ((U + 0.5) * 800), px_time);
    }
    //write Y1
    for (double Y: YLine1) {
        m_mod_ptr->write_tone(1500 + (Y * 800), px_time);
    }

    for (int i = 0; i < YLine0.size(); ++i) {
        m_img_ptr_n->write_pixel(i, img_line, YLine0[i], avgU[i], avgV[i]);
        m_img_ptr_n->write_pixel(i, img_line + 1, YLine1[i], avgU[i], avgV[i]);
    }
}

std::vector<double> pdModeEnc::calculateAverageVector(std::vector<double> v1, std::vector<double> v2) const {
    std::vector<double> avgVector;
    //pre-allocate vector
    avgVector.reserve(v1.size());

    for (int i = 0; i < v1.size(); ++i) {
        avgVector.push_back((v1[i] + v2[i]) / 2);
    }
    return avgVector;
}