//
// Created by atisha on 8/3/18.
//

#ifndef MY_OWN_PD_MODE_SYNTH_H
#define MY_OWN_PD_MODE_SYNTH_H

#include "Demodulator.h"
#include <string>
#include <fstream>

#define TWO_PI 6.283185307179586476925286766559
#define SAMPLERATE 32000

/**
 * @brief writes a word to m_output in little endian
 */
template <typename Word>
void write_word(std::ostream* outs, Word value, unsigned size = sizeof( Word ) );

class Modulator {
public:

    Modulator(const std::string wav_filename);

    ~Modulator() {delete m_output;}

    void finalize();

    void test();

    /**
     * @brief write a tone to m_output for a certain duration
     * @param frequency the frequency of the tone in hertz
     * @param duration the duration of te tone in microseconds
     */
    void write_tone(const double frequency, const int duration);

    Demodulator* demodulator;

private:

    std::ofstream* m_output;
    size_t m_data_chunk_pos;
    int m_bits_per_sample =  16;
    int m_channels =          1; //1 or 2
    int m_volume =        23768;
    double m_current_sample = 0; //keep track of the current sample (to calculate sin())
    double         m_offset = 0; //offset for the sin wave
    double              m_spmus; //samples per microsecond
    double             m_factor; //factor to multiply value with (in sin())

};

#endif //MY_OWN_PD_MODE_SYNTH_H
