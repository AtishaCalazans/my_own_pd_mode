//
// Created by atisha on 8/2/18.
//

#include "ImageWrapper.h"
#include <iostream>

ImageWrapper::ImageWrapper(std::string filename) {
    //m_image.read(filename);
    m_image2.read(filename);
}

ImageWrapper::ImageWrapper(int width, int height) {
    //m_image = Image(Geometry(width, height), Color("black"));
    m_image2 = png::image<png::rgb_pixel>(width, height);
}

std::vector<double> ImageWrapper::getYLine(int line) const {
    std::vector<double> YLine;
    for (int i = 0; i < getWidth(); ++i) {
        png::rgb_pixel rgb = m_image2.get_pixel(i, line);
        YUV yuv = RGB_to_YUV(rgb);
        YLine.emplace_back(yuv.y);
    }
    return YLine;
//    std::vector<double> YLine;
//    for (int i = 0; i < getWidth(); ++i) {
//        ColorYUV YUV = m_image.pixelColor(i, line);
//        YLine.emplace_back(YUV.y());
//    }
//    return YLine;
}

std::vector<double> ImageWrapper::getULine(int line) const {
    std::vector<double> ULine;
    for (int i = 0; i < getWidth(); ++i) {
        png::rgb_pixel rgb = m_image2.get_pixel(i, line);
        YUV yuv = RGB_to_YUV(rgb);
        ULine.emplace_back(yuv.u);
    }
    return ULine;
//    std::vector<double> ULine;
//    for (int i = 0; i < getWidth(); ++i) {
//        ColorYUV YUV = m_image.pixelColor(i, line);
//        ULine.emplace_back(YUV.u());
//    }
//    return ULine;

}

std::vector<double> ImageWrapper::getVLine(int line) const {
    std::vector<double> VLine;
    for (int i = 0; i < getWidth(); ++i) {
        png::rgb_pixel rgb = m_image2.get_pixel(i, line);
        YUV yuv = RGB_to_YUV(rgb);
        VLine.emplace_back(yuv.v);
    }
    return VLine;
//    std::vector<double> VLine;
//    for (int i = 0; i < getWidth(); ++i) {
//        ColorYUV YUV = m_image.pixelColor(i, line);
//        VLine.emplace_back(YUV.v());
//    }
//    return VLine;

}

size_t ImageWrapper::getHeight() const {
//    return m_image.size().height();
    return m_image2.get_height();
}

size_t ImageWrapper::getWidth() const {
//    return m_image.size().width();
    return m_image2.get_width();
}

void ImageWrapper::write_pixel(int xp, int yp, double y, double u, double v) {
//    ColorYUV new_color(y, u, v);
    m_image2.set_pixel(xp, yp, YUV_to_RGB({y, u, v}));
}

void ImageWrapper::write_to_file(std::string filename) {
    //m_image.write(filename);
    m_image2.write(filename);
}

ImageWrapper::YUV ImageWrapper::RGB_to_YUV(const png::rgb_pixel& rgb) const {
    double r =   double(rgb.red) / 255.0;
    double g = double(rgb.green) / 255.0;
    double b =  double(rgb.blue) / 255.0;
    double y =    0.299 * r    +  0.587 * g  +   0.114 * b;
    double u = -0.14713 * r  -  0.28886 * g  +   0.436 * b;
    double v =    0.615 * r  -  0.51499 * g  - 0.10001 * b;
    return {y, u, v};
}

png::rgb_pixel ImageWrapper::YUV_to_RGB(const ImageWrapper::YUV &yuv) const {
    double r = yuv.y + 1.13983 * yuv.v;
    double g = yuv.y - 0.39465 * yuv.u - 0.58060 * yuv.v;
    double b = yuv.y + 2.03211 * yuv.u;
    r = r > 1 ? 1 : r;
    g = g > 1 ? 1 : g;
    b = b > 1 ? 1 : b;
    r = r < 0 ? 0 : r;
    g = g < 0 ? 0 : g;
    b = b < 0 ? 0 : b;
    return png::rgb_pixel(int(r*255), int(g*255), int(b*255));
}
