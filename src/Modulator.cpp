//
// Created by atisha on 8/3/18.
//
#include <cmath>
#include <iostream>
#include "Modulator.h"

template <typename Word>
void write_word(std::ostream* outs, Word value, unsigned int size) {
    for (; size; --size, value >>= 8)
      outs->put(static_cast<char>(value & 0xFF));
}

Modulator::Modulator(const std::string wav_filename) {
    m_output = new std::ofstream(wav_filename, std::ios::binary);
    m_spmus = SAMPLERATE / 1000000.0;
    m_factor = TWO_PI / SAMPLERATE;

    // Write the file headers
    *m_output << "RIFF----WAVEfmt "; // (chunk size to be filled in later)
    write_word(m_output,                                                  16, 4);  // no extension data
    write_word(m_output,                                                   1, 2);  // PCM - integer samples
    write_word(m_output,                                          m_channels, 2);  // number of channels
    write_word(m_output,                                          SAMPLERATE, 4);  // sample rate
    write_word(m_output,   (SAMPLERATE * m_bits_per_sample * m_channels) / 8, 4);  // byte rate
    write_word(m_output,                (m_channels * m_bits_per_sample) / 8, 2);  // data block size
    write_word(m_output,                                  m_bits_per_sample, 2 );  // number of bits per sample

    // Write the data chunk header
    m_data_chunk_pos = m_output->tellp();
    *m_output << "data----";  // (chunk size to be filled in later)
}

void Modulator::finalize() {
    // We'll need the final file size to fix the chunk sizes in the header
    size_t file_length = m_output->tellp();

    // Fix the data chunk header to contain the data size
    m_output->seekp(m_data_chunk_pos + 4);
    write_word(m_output, file_length - m_data_chunk_pos + 8);

    // Fix the file header to contain the proper RIFF chunk size, which is (file size - 8) bytes
    m_output->seekp(0 + 4);
    write_word(m_output, file_length - 8, 4);
}

void Modulator::test() {
    // Write the audio samples
    // (We'll generate a single C4 note with a sine wave, fading from left to right)
    double two_pi = 6.283185307179586476925286766559;
    double max_amplitude = m_volume;

    double hz = SAMPLERATE;    // samples per second
    double frequency = 261.626;  // middle C
    double seconds = 2.5;      // time

    int N = hz * seconds;  // total number of samples
    for (int n = 0; n < N; n++) {
        double amplitude = (double) n / N * max_amplitude;
        double value = sin((two_pi * n * frequency) / hz);
        write_word(m_output, (int) (amplitude * value), 2);
    }
}

void Modulator::write_tone(const double frequency, const int duration) {
    m_current_sample += m_spmus * duration;
    int samples = int(m_current_sample);
    double freq_factor = frequency * m_factor;
    double value;

    for (int i = 0; i < samples; ++i) {
        value = sin(i * freq_factor + m_offset);
        if (demodulator->active) {
            demodulator->run(value);
        }
        write_word(m_output, int(m_volume * value), 2);
    }

    m_offset += (samples) * freq_factor;
    m_current_sample -= samples;

}