//
// Created by atisha on 8/2/18.
//

#ifndef MY_OWN_PD_MODE_IMAGEWRAPPER_H
#define MY_OWN_PD_MODE_IMAGEWRAPPER_H

#include <Magick++.h>
#include <png++/png.hpp>
#include <string>
#include <vector>

using namespace Magick;

/*
 * @brief A class that wraps around the Magick::Image class
 */
class ImageWrapper {
public:

    struct YUV {
        double y;
        double u;
        double v;
    };

    ImageWrapper(std::string filename);

    ImageWrapper(int width, int height);

    ~ImageWrapper()=default;

    std::vector<double> getYLine(int line) const;

    std::vector<double> getULine(int line) const;

    std::vector<double> getVLine(int line) const;

    size_t getHeight() const;

    size_t getWidth() const;

    /**
     * @brief write the yuv values to a pixel
     * @param xp x position of the pixel
     * @param yp y position of the pixel
     * @param y Y color value
     * @param u U color value
     * @param v V color value
     */
    void write_pixel(int xp, int yp, double y = 0, double u = 0, double v = 0);

    void write_to_file(std::string filename);

private:

    Image m_image;
    png::image<png::rgb_pixel> m_image2;

    YUV RGB_to_YUV(const png::rgb_pixel& rgb) const;

    png::rgb_pixel YUV_to_RGB(const YUV &yuv) const;

};

#endif //MY_OWN_PD_MODE_IMAGEWRAPPER_H
