//
// Created by atisha on 06.09.18.
//

#ifndef MY_OWN_PD_MODE_BASEDECODER_H
#define MY_OWN_PD_MODE_BASEDECODER_H

#include "../ModeTable.h"
#include <memory>
#include <array>
#include <vector>

class ImageWrapper;

class BaseDecoder {
public:

    BaseDecoder(sstv_mode mode);

    void evaluate_sample(double sample);

    void run(double sample);

    void write_image(std::string filename);

protected:

    std::unique_ptr<ImageWrapper> m_image_ptr;
    sstv_mode m_mode;

    std::vector<double> m_Y0;
    std::vector<double> m_V;
    std::vector<double> m_U;
    std::vector<double> m_Y1;
    int m_current_sample = 0;
    int m_current_x = 0;
    int m_current_y = 0;
    int m_current_component = 0; // Y0: 0 | V: 1 | U: 2 | Y1: 3
    std::vector<double> m_current_pixel_values;

    void write_lines();

};

#endif //MY_OWN_PD_MODE_BASEDECODER_H
