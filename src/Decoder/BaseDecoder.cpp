//
// Created by atisha on 06.09.18.
//
#include "BaseDecoder.h"
#include "../Modulator.h"
#include "../ImageWrapper.h"
#include <cmath>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iostream>

BaseDecoder::BaseDecoder(sstv_mode mode) : m_mode(mode) {
    m_image_ptr = std::make_unique<ImageWrapper>(MODE_TABLE[m_mode].res_x, MODE_TABLE[m_mode].res_y);
}

void BaseDecoder::run(double sample) {
    double component_line_time = MODE_TABLE[m_mode].res_x * MODE_TABLE[m_mode].pixel_time;
    double current_time = (double(m_current_sample) * (1000000.0 / double(SAMPLERATE)));  // in microseconds

    double current_line_start = m_current_y * component_line_time * 4;
    double current_line_end = (m_current_y + 1) * component_line_time * 4;

    double current_component_start = current_line_start + (m_current_component * component_line_time);
    double current_component_end = current_line_start + ((m_current_component + 1) * component_line_time);

    // if the end of the line is reached
    if (current_time > current_line_end) {
        // color the pixels for this scanline
        write_lines();
        // if the full image has been drawn, save file and exit
        if (m_current_y == (MODE_TABLE[m_mode].res_y / 2) - 2) {
            write_image("test_image.png");
            return;
        }
        m_current_x = 0;
    } else if (current_time > current_component_end) {
        ++m_current_component;
        m_current_x = 0;
    }

    // if the end of the current pixel is reached
    double current_pixel_time = current_component_start + ((m_current_x + 1) * MODE_TABLE[m_mode].pixel_time);
    if (current_time > current_pixel_time) {
        double avg = 0;
        for (const double& val: m_current_pixel_values) {
            avg += val;
        }
        avg = avg / m_current_pixel_values.size();

        switch (m_current_component) {
            case 0:
                m_Y0.push_back(avg);
                break;
            case 1:
                m_V.push_back(avg - 0.5);
                break;
            case 2:
                m_U.push_back(avg - 0.5);
                break;
            case 3:
                m_Y1.push_back(avg);
                break;
            default:
                break;
        }

        m_current_pixel_values.clear();
        ++m_current_x;
    }

    m_current_pixel_values.push_back(sample);
    ++m_current_sample;
}

void BaseDecoder::write_lines() {

    for (int i = 0; i < MODE_TABLE[m_mode].res_x; ++i) {
            m_image_ptr->write_pixel(i, 2 * m_current_y, m_Y0[i], m_U[i], m_V[i]);
            m_image_ptr->write_pixel(i, 2 * m_current_y + 1, m_Y1[i], m_U[i], m_V[i]);
    }

    m_Y0.clear();
    m_V.clear();
    m_U.clear();
    m_Y1.clear();
    ++m_current_y;
    m_current_component = 0;
}

void BaseDecoder::write_image(std::string filename) {
    write_lines();
    m_image_ptr->write_to_file(filename);
}