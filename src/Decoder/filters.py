import sys
import math
import matplotlib.pyplot as plt

samplerate  = 32000
taps        = 51
#cutoff      = samplerate / 10
cutoff      = 800
M           = taps - 1              # filter order
ft          = cutoff / samplerate   # normalised transition frequency

coefficients = list()

def calculate_coefficients():
    for i in range(taps):
        if i == (M / 2):
            coefficients.append(2 * ft)
        else:
            coefficients.append(math.sin(2 * math.pi * ft * (i - M/2)) / (math.pi * (i - M/2)))

def barlett_window(coeffs):
    for i in range(len(coeffs)):
        weight = 1 - (2 * abs(i - M / 2)) / M
        coeffs[i] = coeffs[i] * weight

    return coeffs

def hanning_window(coeffs):
    for i in range(len(coeffs)):
        weight = 0.5 - 0.5 * math.cos((2 * math.pi * i) / M)
        coeffs[i] = coeffs[i] * weight

    return coeffs

def hamming_window(coeffs):
    for i in range(len(coeffs)):
        weight = 0.54 - 0.46 * math.cos((2 * math.pi * i) / M)
        coeffs[i] = coeffs[i] * weight

    return coeffs

def blackman_window(coeffs):
    for i in range(len(coeffs)):
        weight = 0.42 - 0.5 * math.cos((2 * math.pi * i) / M) + 0.08 * math.cos((4 * math.pi * i) / M)
        coeffs[i] = coeffs[i] * weight

    return coeffs

def default_example():
    return [+0.0001082461, +0.0034041195, +0.0063570207, +0.0078081648,
            +0.0060550614, -0.0002142384, -0.0104500335, -0.0211855480,
            -0.0264527776, -0.0201269304, +0.0004419626, +0.0312014771,
            +0.0606261038, +0.0727491887, +0.0537028370, -0.0004362161,
            -0.0779387981, -0.1511168919, -0.1829049634, -0.1390189257,
            -0.0017097774, +0.2201896764, +0.4894395006, +0.7485289338,
            +0.9357596142, +1.0040320616, +0.9357596142, +0.7485289338,
            +0.4894395006, +0.2201896764, -0.0017097774, -0.1390189257,
            -0.1829049634, -0.1511168919, -0.0779387981, -0.0004362161,
            +0.0537028370, +0.0727491887, +0.0606261038, +0.0312014771,
            +0.0004419626, -0.0201269304, -0.0264527776, -0.0211855480,
            -0.0104500335, -0.0002142384, +0.0060550614, +0.0078081648,
            +0.0063570207, +0.0034041195, +0.0001082461]

if __name__ == "__main__":
    # filled in if the argument was specified
    window = ""
    graph = ""
    write = ""

    window_args = ["-default", "-bar", "-han", "-ham", "-blackm"]
    graph_args = ["-graph"]
    write_args = ["-write"]

    for i in range(1, len(sys.argv)):
        arg = sys.argv[i]

        if arg in window_args:
            if not window:
                window = arg
            else:
                print("\033[91mOnly 1 window argument is allowed\033[0m")
                raise SystemExit

        elif arg in graph_args:
            if not graph:
                graph = arg
            else:
                print("\033[91mOnly 1 graph argument is allowed\033[0m")
                raise SystemExit

        elif arg in write_args:
            if not write:
                write = arg
            else:
                print("\033[91mOnly 1 write argument is allowed\033[0m")
                raise SystemExit

        else:
            print("\033[91mInvalid argument \""+ arg +"\"\033[0m")
            raise SystemExit


    calculate_coefficients()
    windowed_coeffs = list()

    if window == "" or window == "-default":
        windowed_coeffs = default_example()
    elif window == "-bar":
        windowed_coeffs = barlett_window(coefficients[:])
    elif window == "-han":
        windowed_coeffs = hamming_window(coefficients[:])
    elif window == "-ham":
        windowed_coeffs = hamming_window(coefficients[:])
    elif window == "-blackm":
        windowed_coeffs = blackman_window(coefficients[:])

    if graph == "-graph":
        plt.subplot(2, 1, 1)
        plt.plot([i for i in range(taps)], coefficients, 'o-')
    
        plt.subplot(2, 1, 2)
        plt.plot([i for i in range(taps)], windowed_coeffs, 'o-')

        plt.show()

    if write == "-write":
        coeff_file = open("filter_coefficients.txt", "w")
        for coefficient in windowed_coeffs:
            coeff_file.write(str(coefficient) + '\n')

        coeff_file.close()

    sum = 0
    for x in windowed_coeffs:
        sum += x

    print(sum)