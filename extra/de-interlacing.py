from PIL import Image

im = Image.open("interlaced.png")
new_im = Image.new( 'RGB', im.size, "black")

pixels = im.load()
new_pixels = new_im.load()

for i in range(int(im.size[1] / 2)):
    for j in range(im.size[0]):
        new_pixels[j, i] = pixels[j, 2*i]
        new_pixels[j, int(im.size[1] / 2) + i] = pixels[j, 2*i + 1]

new_im.show()
new_im.save("de-interlaced.png")
        
